# ref https://kairos.io/docs/reference/image_matrix/
FROM quay.io/kairos/debian:bookworm-standard-amd64-generic-v2.5.0-k3sv1.29.0-k3s1

# Pre-reqs for Longhorn
RUN apt-get -y update && apt-get -y install open-iscsi 

RUN export VERSION="kairos-bookworm-longhorn"
RUN envsubst '${VERSION}' </etc/os-release